[![Managecat](images/prometheus-stacked-color.svg)](../images)
## Jelastic Node-exporter Add-on

This repository provides [NodeExporter](https://prometheus.io/docs/guides/node-exporter/) add-on for Jelastic Platform.


**Type of nodes this add-on can be applied to**:
- nginxphp-dockerized
- nginx-dockerized 
- nginx
- nginxphp  
- apache
- apache2

### What it can be used for?

  
### Deployment
