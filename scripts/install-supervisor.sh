#!/bin/bash -e

FILE=/etc/supervisord.conf
if [ ! -f "$FILE" ]; then

  yum install supervisor -y

  wget -q "https://bitbucket.org/akki-team/node-exporter/raw/master/config/supervisor/node_exporter.ini" -O "/etc/supervisord.d/node_exporter.ini"

  systemctl start supervisord
  systemctl enable supervisord
  supervisorctl reread
  supervisorctl update
fi
