#!/bin/bash -e

rm /etc/supervisor.d/conf.d/node_exporter.conf

supervisorctl reread
supervisorctl update

rm -rf /opt/node_exporter
